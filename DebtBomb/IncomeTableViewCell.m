//
//  IncomeTableViewCell.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//


#import "IncomeTableViewCell.h"
#import "DebtBombData.h"
#import "IncomesViewController.h"
#import "DebtBombNotifications.h"
#import "FamilyInfoTableViewCell.h"

#define EXTRA_MAX_INCOME_ROOM 1.1
#define DEFAULT_MAX_INCOME 100000

@implementation IncomeTableViewCell

@synthesize slider = _slider;
@synthesize yearLabel = _yearLabel;
@synthesize incomeLabel = _incomeLabel;
@synthesize row = _row;
@synthesize incomeType = _incomeType;
@synthesize minLabel = _minLabel;
@synthesize maxLabel = _maxLabel;
@synthesize detailedIncomeController = _detailedIncomeController;


- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.slider = nil;
    self.yearLabel = nil;
    self.incomeLabel = nil;
    self.minLabel = nil;
    self.maxLabel = nil;
    self.incomeType = nil;
    self.detailedIncomeController = nil;
    [super dealloc];
}

- (void) setIncomeFromPicker:(NSInteger) newIncome{
    [self updateMaxIncomeIfNecessary:newIncome];
    [self.slider setValue:newIncome animated:NO];
    [self sliderChanged:nil];
    [self showIncome:newIncome];
}

- (void) maxIncomeChanged:(NSNotification*) notification {
    NSNumber* newMaxIncome = (NSNumber*) notification.object;
    [self setMaxIncome:[newMaxIncome intValue]];
}

- (void) marriageNotification:(NSNotification*)notification {
    FamilyInfoTableViewCell* cell = (FamilyInfoTableViewCell*)notification.object;
    if (cell.row < self.row) {
        //[self.slider. setOn:[cell.marriedSwitch isOn]];
    }
}

- (void) setMaxIncome:(NSInteger) newMaxIncome
{
    newMaxIncome = newMaxIncome * EXTRA_MAX_INCOME_ROOM;
    if (newMaxIncome > 2000000) {
        newMaxIncome = 2000000;
    }
    self.maxLabel.text = [NSString stringWithFormat:@"$%dk", newMaxIncome / 1000];
    self.slider.maximumValue = newMaxIncome;
    [self.slider setValue:self.slider.value + 0.1 animated:YES];
    [self.slider setValue:self.slider.value - 0.1 animated:YES];

    [self showIncome:self.slider.value];
}

- (void)updateMaxIncomeIfNecessary:(float) income {
    if (lroundf(income) >= lroundf(self.slider.maximumValue)) {
        NSNumber* newMaxIncome = [NSNumber numberWithFloat:(income)];
        if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:PRIMARY_MAX_INCOME_CHANGED object:newMaxIncome];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:SPOUSE_MAX_INCOME_CHANGED object:newMaxIncome];
        }
    } else {
        //get the max income in all 10 years and set the max income lower if we need to
        NSMutableArray *incomes;
        if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE ]) {
            incomes = [[[DebtBombData sharedInstance] userInputs] objectForKey:PRIMARY_INCOME_INPUT]; 
        } else {
            incomes = [[[DebtBombData sharedInstance] userInputs] objectForKey:SPOUSE_INCOME_INPUT]; 
        } 
        NSNumber *maxNumber = [incomes valueForKeyPath:@"@max.intValue"];
        NSInteger max = [maxNumber intValue];
        if (max < (DEFAULT_MAX_INCOME/EXTRA_MAX_INCOME_ROOM)) {
            max = ceilf(DEFAULT_MAX_INCOME/EXTRA_MAX_INCOME_ROOM);
            maxNumber = [NSNumber numberWithInt:max];
        }
        if (self.slider.maximumValue > (max * EXTRA_MAX_INCOME_ROOM)) {
            if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:PRIMARY_MAX_INCOME_CHANGED object:maxNumber];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:SPOUSE_MAX_INCOME_CHANGED object:maxNumber];
            }        
        }
        
    }
}

- (IBAction)sliderChanged:(id)sender {
    
    //save directly to the debt bomb data user inputs here
    NSMutableArray *incomes;
    if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE ]) {
        incomes = [[[DebtBombData sharedInstance] userInputs] objectForKey:PRIMARY_INCOME_INPUT]; 
    } else {
        incomes = [[[DebtBombData sharedInstance] userInputs] objectForKey:SPOUSE_INCOME_INPUT]; 
    }
    [incomes replaceObjectAtIndex:self.row withObject:[NSNumber numberWithFloat:self.slider.value]];
    
    [self showIncome:self.slider.value];
    [self updateMaxIncomeIfNecessary:self.slider.value];
}

- (void) showIncome:(float) incomeFloat {
    // alloc formatter
    NSNumberFormatter *currencyStyle = [[[NSNumberFormatter alloc] init] autorelease];
    
    // set options.
    [currencyStyle setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyStyle setMaximumFractionDigits:0];
    
    NSNumber *income = [NSNumber numberWithFloat:incomeFloat];
    
    self.incomeLabel.text = [currencyStyle stringFromNumber:income];
}

- (void)startListeningToNotifications
{
    if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maxIncomeChanged:) name:PRIMARY_MAX_INCOME_CHANGED object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(maxIncomeChanged:) name:SPOUSE_MAX_INCOME_CHANGED object:nil];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

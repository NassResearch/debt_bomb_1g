//
//  CongressPersonViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CongressPersonViewController : UITableViewController
@property (nonatomic, strong) NSDictionary* congressPerson;
@property (nonatomic, strong) UIView* headerView;
@end

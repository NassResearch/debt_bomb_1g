//
//  DetailedIncomeController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class IncomeTableViewCell;

@interface DetailedIncomeController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *incomeTextField;
@property (nonatomic, strong) IBOutlet UINavigationItem *navItem;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) IncomeTableViewCell *cell;

- (IBAction)done:(id)sender;
@end

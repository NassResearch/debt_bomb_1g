//
//  DetailedIncomeController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DetailedIncomeController.h"
#import "IncomeTableViewCell.h"
#import "DebtBombNotifications.h"
#import "IncomesViewController.h"

@implementation DetailedIncomeController
@synthesize incomeTextField = _incomeTextField;
@synthesize descriptionLabel = _descriptionLabel;
@synthesize navItem = _navItem;
@synthesize cell = _cell;

- (void) dealloc {
    self.incomeTextField = nil;
    self.descriptionLabel = nil;
    self.navItem = nil;
    self.cell = nil;
    [super dealloc];
}

- (void) setIncome:(NSInteger) income {
    /*NSNumberFormatter *formatter = [[[NSNumberFormatter alloc] init] autorelease];
    [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [formatter setNumberStyle: NSNumberFormatterCurrencyStyle];  
    self.incomeTextField.text = [formatter stringFromNumber: [formatter numberFromString:self.incomeTextField.text]];*/
    self.incomeTextField.text = [NSString stringWithFormat:@"%d",income];
}


- (IBAction)done:(id)sender {
    NSInteger income = [self.incomeTextField.text intValue];
    [self.cell setIncomeFromPicker:income];
    [[NSNotificationCenter defaultCenter] postNotificationName:INCOME_CHANGED object:self];
    [self dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setIncome:self.cell.slider.value];
    if ([self.cell.incomeType isEqualToString:PRIMARY_INCOME_TYPE]) {
        self.navItem.title = @"Primary Income";
        self.descriptionLabel.text = [NSString stringWithFormat:@"Primary Income for %@",self.cell.yearLabel.text];
    } else {
        self.navItem.title = @"Spouse Income";
        self.descriptionLabel.text = [NSString stringWithFormat:@"Spouse Income for %@",self.cell.yearLabel.text];
    }
    [self.incomeTextField becomeFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

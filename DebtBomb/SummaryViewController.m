//
//  SummaryViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "SummaryViewController.h"
#import "BackgroundViewController.h"
#import "CongressStatesViewController.h"
#import "ExploreGraphViewController.h"

@implementation SummaryViewController
@synthesize extraTaxesLabel = _extraTaxesLabel;
@synthesize webView = _webView;

- (IBAction)showBackground:(id)sender {
    BackgroundViewController *backgroundViewController = [[[BackgroundViewController alloc] initWithNibName:@"BackgroundViewController" bundle:nil] autorelease];
    backgroundViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:backgroundViewController animated:YES];
}
- (IBAction)showContactCongress:(id)sender {
    CongressStatesViewController *congressStatesViewController = [[[CongressStatesViewController alloc] initWithNibName:@"CongressStatesViewController" bundle:nil] autorelease];
    [self.navigationController pushViewController:congressStatesViewController animated:YES];
}

- (void) dealloc {
    self.extraTaxesLabel = nil;
    self.webView = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Summary";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.webView loadHTMLString:@"<h1>Some HTML goes here</h1>" 
                         baseURL:[NSURL URLWithString:@"http://www.debtbomb.com/"]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    ExploreGraphViewController *exploreGraphViewController = [[[ExploreGraphViewController alloc] initWithNibName:@"ExploreGraphViewController" bundle:nil] autorelease];
    exploreGraphViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentModalViewController:exploreGraphViewController animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

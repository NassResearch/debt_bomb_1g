//
//  StateViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StateViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) IBOutlet UIPickerView *statePicker;
@property (nonatomic, strong) IBOutlet UILabel *stateLabel;

-(IBAction)done:(id)sender;
+ (NSString*) stateForRow:(NSInteger)row;


@end

//
//  FamilyInfoViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FamilyInfoTableViewCell.h"

@interface FamilyInfoViewController : UIViewController
@property (nonatomic, strong) IBOutlet FamilyInfoTableViewCell *familyCell;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@end

//
//  DebtBombNotifications.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#define AGE_CHANGED @"AGE_CHANGED"
#define STATE_CHANGED @"STATE_CHANGED"
#define PRIMARY_MAX_INCOME_CHANGED @"PRIMARY_MAX_INCOME_CHANGED"
#define SPOUSE_MAX_INCOME_CHANGED @"SPOUSE_MAX_INCOME_CHANGED"
#define INCOME_CHANGED @"INCOME_CHANGED"
#define MARRIAGE_CHANGED @"MARRIAGE_CHANGED"
#define KIDS_CHANGED @"KIDS_CHANGED"
//
//  FamilyInfoViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FamilyInfoViewController.h"
#import "DebtBombData.h"
#import "MNStepper.h"

@implementation FamilyInfoViewController

@synthesize familyCell = _familyCell;
@synthesize tableView = _tableView;


- (IBAction)done:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}


- (void) dealloc {
    self.familyCell = nil;
    self.tableView = nil;
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FamilyInfoTableViewCell";
    
    FamilyInfoTableViewCell *cell = (FamilyInfoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"FamilyInfoTableViewCell" owner:self options:nil];
        cell = self.familyCell;
        self.familyCell = nil;
    }
    
    // Configure the cell...
    cell.yearLabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 2011];
    cell.row = indexPath.row;
    NSUInteger numberOfKids = [[[[[DebtBombData sharedInstance] userInputs] objectForKey:KIDS_BY_YEAR] objectAtIndex:indexPath.row] intValue];
    
    //configure the stepper in code
    CGRect stepperFrame = CGRectMake(cell.contentView.bounds.size.width - 114, (NSUInteger)(cell.contentView.frame.size.height - 70)/2+2, 94, 70);
    cell.childStepper = [[[MNStepper alloc] initWithFrame:stepperFrame] autorelease];
    [cell.childStepper setNeedsLayout];
    [cell.contentView addSubview:cell.childStepper];
    cell.childStepper.value = numberOfKids;
    [cell.childStepper addTarget:cell action:@selector(kidsValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    cell.marriedSwitch.onText = @"Yes";
    cell.marriedSwitch.offText = @"No";
    
    [cell.marriedSwitch setOn:[[[[[DebtBombData sharedInstance] userInputs] objectForKey:MARRIED_BY_YEAR] objectAtIndex:indexPath.row] boolValue]];
    [cell startListeningToNotifications];
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


@end

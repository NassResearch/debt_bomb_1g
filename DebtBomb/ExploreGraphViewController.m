//
//  ExploreGraphViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ExploreGraphViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation ExploreGraphViewController
@synthesize navBar = _navBar;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self showNavBar:nil];
    
}

- (void) fadeNavBar {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:4.0];
    self.navBar.alpha = 0.0;
    [UIView commitAnimations];  
}

- (IBAction)showNavBar:(id)sender {
    self.navBar.alpha = 1.0;
    [self performSelector:@selector(fadeNavBar) withObject:nil afterDelay:4.0];
}

- (IBAction)done:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void) viewDidAppear:(BOOL)animated {
    [self fadeNavBar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

@end

//
//  GraphViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphViewController : UIViewController
- (IBAction)showSummary:(id)sender; 
- (IBAction)exploreGraph:(id)sender;

@end

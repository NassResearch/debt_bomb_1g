//
//  CongressStateViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CongressStateViewController.h"
#import "CongressPersonViewController.h"

#define SENATE_SECTION 0
#define HOUSE_SECTION 1

@implementation CongressStateViewController
@synthesize state = _state;
@synthesize stateName = _stateName;

- (void) dealloc {
    self.state = nil;
    self.stateName = nil;
    [super dealloc];
}

- (NSArray*) houseMembers {
    NSString *stateName = [self.state objectForKey:@"name"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@",[stateName uppercaseString]];
    return [[self.state allKeys] filteredArrayUsingPredicate:filter];
}

//- (NSArray*) houseMembersSortedAlphabetically {
   // NSArray *housePeeps = [self.state objectsForKeys:[self houseMembers] notFoundMarker:[NSNull NULL]];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"interest"  ascending:YES];

    
//}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.stateName;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([[self.state objectForKey:@"name"] isEqualToString:@"District of Columbia"]) {
        return @"Delegate";
    }
    if (section == SENATE_SECTION) {
        return @"Senate";
    } else {
        return @"House";
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([[self.state objectForKey:@"name"] isEqualToString:@"District of Columbia"]) {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.state objectForKey:@"name"] isEqualToString:@"District of Columbia"]) {
        return 1;
    }
    if (section == SENATE_SECTION) {
        return 2;
    } else {
        return [[self houseMembers] count];
    }
}

- (NSDictionary *)congressPerson:(NSIndexPath *)indexPath
{
    NSDictionary *congressPerson;
    NSString *stateName = [self.state objectForKey:@"name"];
    if ([stateName isEqualToString:@"District of Columbia"]) {
        congressPerson = [self.state objectForKey:@"DC_00"];
    } else {
        if (indexPath.section == SENATE_SECTION) {
            if (indexPath.row == 0) {
                congressPerson = [self.state objectForKey:@"SR_SENATOR"];
            } else {
                congressPerson = [self.state objectForKey:@"JR_SENATOR"];
            }
        } else {
            NSString *keyName = nil;
            if (indexPath.row < 10) {
                keyName = [NSString stringWithFormat:@"%@_0%d",[stateName uppercaseString],indexPath.row];
            } else {
                keyName = [NSString stringWithFormat:@"%@_%d",[stateName uppercaseString],indexPath.row];
            }
            congressPerson = [self.state objectForKey:keyName];
        }
    }
    return congressPerson;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    NSDictionary *congressPerson = nil;
    congressPerson = [self congressPerson:indexPath];
    cell.textLabel.text = [congressPerson objectForKey:@"name"];
    cell.detailTextLabel.text = [congressPerson objectForKey:@"district"];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.

    CongressPersonViewController *congressPersonViewController = [[[CongressPersonViewController alloc] initWithNibName:@"CongressPersonViewController" bundle:nil] autorelease];
    // ...
    // Pass the selected object to the new view controller.
    congressPersonViewController.congressPerson = [self congressPerson:indexPath];
    [self.navigationController pushViewController:congressPersonViewController animated:YES];
     
}

@end

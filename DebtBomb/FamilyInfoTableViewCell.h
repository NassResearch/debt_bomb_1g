//
//  FamilyInfoTableViewCell.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MNStepper.h"
#import "DCRoundSwitch.h"

@interface FamilyInfoTableViewCell : UITableViewCell
@property (nonatomic, assign) NSUInteger row;
@property (nonatomic, strong) IBOutlet UILabel *yearLabel;
@property (nonatomic, strong) IBOutlet DCRoundSwitch *marriedSwitch;
@property (nonatomic, strong) MNStepper *childStepper;

- (IBAction)kidsValueChanged:(id)sender;
- (IBAction)marriageChanged:(id)sender;
- (void) startListeningToNotifications;

@end

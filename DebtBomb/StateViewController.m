//
//  StateViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "StateViewController.h"
#import "DebtBombData.h"
#import "DebtBombNotifications.h"

@implementation StateViewController

@synthesize stateLabel = _stateLabel;
@synthesize statePicker = _statePicker;


- (void) dealloc {
    self.stateLabel = nil;
    self.statePicker = nil;
}

+ (NSString*) stateForRow:(NSInteger)row {
    return [[[DebtBombData sharedInstance] states] objectAtIndex:row];
}


- (IBAction)done:(id)sender {
    //save picked value
    NSInteger selectedRow = [self.statePicker selectedRowInComponent:0];
    [[[DebtBombData sharedInstance] userInputs] setObject:[NSNumber numberWithInt:selectedRow] forKey:STATE_INDEX];
    
    //tell anyone listening
    [[NSNotificationCenter defaultCenter] postNotificationName:STATE_CHANGED object:self];
    
    [self dismissModalViewControllerAnimated:YES];
}


- (void) changeStateLabelText:(NSInteger)row {
    self.stateLabel.text = [NSString stringWithFormat:@"State: %@",[StateViewController stateForRow:row]];
}


#pragma mark - UIPickerView Data Source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[[DebtBombData sharedInstance] states] count];
}


#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [StateViewController stateForRow:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self changeStateLabelText:row];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSInteger stateIndex = [[[[DebtBombData sharedInstance] userInputs] objectForKey:STATE_INDEX] intValue]; 
    [self changeStateLabelText:stateIndex];
    [self.statePicker selectRow:stateIndex inComponent:0 animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

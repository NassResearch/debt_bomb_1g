//
//  IncomesViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "IncomesViewController.h"
#import "DebtBombData.h"
#import "DebtBombNotifications.h"

@implementation IncomesViewController
@synthesize incomeCell = _incomeCell;
@synthesize navItem = _navItem;
@synthesize tableView = _tableView;
@synthesize incomeType = _incomeType;


- (IBAction)done:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void) updateData:(NSNotification*) notification {
    [self.tableView reloadData];
}

- (void) dealloc {
    self.incomeCell = nil;
    self.navItem = nil;
    self.tableView = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:INCOME_CHANGED object:nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE]) {
        self.navItem.title = @"Primary Income";
    } else {
        self.navItem.title = @"Spouse's Income";
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"IncomeTableViewCell";
    
    IncomeTableViewCell *cell = (IncomeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"IncomeTableViewCell" owner:self options:nil];
        cell = self.incomeCell;
        self.incomeCell = nil;
    }
    
    // Configure the cell...
    cell.yearLabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 2011];
    cell.incomeType = self.incomeType;
    cell.row = indexPath.row;
    [cell startListeningToNotifications];
    cell.detailedIncomeController = [[[DetailedIncomeController alloc] initWithNibName:@"DetailedIncomeController" bundle:nil] autorelease];    
    
    NSDictionary *userInputs = [[DebtBombData sharedInstance] userInputs];
    if ([self.incomeType isEqualToString:PRIMARY_INCOME_TYPE]) {
        NSInteger income = [[[userInputs objectForKey:PRIMARY_INCOME_INPUT] objectAtIndex:indexPath.row] intValue];
        NSInteger max = [[[userInputs objectForKey:PRIMARY_INCOME_INPUT] valueForKeyPath:@"@max.intValue"] intValue];

        if (max > cell.slider.maximumValue) {
            [cell setMaxIncome:max];
        }
        cell.slider.value = income;
    } else {
        NSInteger income = [[[userInputs objectForKey:SPOUSE_INCOME_INPUT] objectAtIndex:indexPath.row] intValue];
        NSInteger max = [[[userInputs objectForKey:SPOUSE_INCOME_INPUT] valueForKeyPath:@"@max.intValue"] intValue];

        if (max > cell.slider.maximumValue) {
            [cell setMaxIncome:max];
        }
        cell.slider.value = income;
    }
    [cell sliderChanged:nil];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    IncomeTableViewCell *cell = (IncomeTableViewCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.detailedIncomeController.cell = cell;    
    [self presentModalViewController:cell.detailedIncomeController animated:YES];
}
@end

//
//  MainInputController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainInputViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
- (void) updateData:(NSNotification*) notification;

@end

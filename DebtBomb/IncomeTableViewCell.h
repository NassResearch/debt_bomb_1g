//
//  IncomeTableViewCell.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailedIncomeController.h"

@interface IncomeTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UISlider *slider;
@property (nonatomic, strong) IBOutlet UILabel *yearLabel;
@property (nonatomic, strong) IBOutlet UILabel *incomeLabel;
@property (nonatomic, strong) IBOutlet UILabel *minLabel;
@property (nonatomic, strong) IBOutlet UILabel *maxLabel;
@property (nonatomic, assign) NSUInteger row;
@property (nonatomic, strong) NSString* incomeType;
@property (nonatomic, strong) DetailedIncomeController* detailedIncomeController;

- (IBAction)sliderChanged:(id)sender;
- (void) showIncome:(float) incomeFloat;
- (void) maxIncomeChanged:(NSNotification*) notification;
- (void) setMaxIncome:(NSInteger) newMaxIncome;
- (void) startListeningToNotifications;
- (void) setIncomeFromPicker:(NSInteger) newIncome;
- (void) updateMaxIncomeIfNecessary:(float) income;
@end

//
//  AgeController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgeViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) IBOutlet UIPickerView *agePicker;
@property (nonatomic, strong) IBOutlet UILabel *ageLabel;

-(IBAction)done:(id)sender;

@end

    //
//  DebtBombData.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DebtBombData.h"
#import "JSONKit.h"

@implementation DebtBombData
@synthesize userInputs = _userInputs;
@synthesize states = _states;
@synthesize congressData = _congressData;
static DebtBombData *sharedData = nil;


+ (DebtBombData*) sharedInstance
{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedData = [[DebtBombData alloc] init]; 
        NSLog(@"creating data");
        NSMutableDictionary *defaults = [[[NSMutableDictionary alloc] init] autorelease];
        [defaults setObject:[NSNumber numberWithInt:18] forKey:AGE_IN_2011];
        [defaults setObject:[NSNumber numberWithInt:0] forKey:STATE_INDEX];
        NSMutableArray *primaryIncomeDefaults = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray *spouseIncomeDefaults = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray *marriageDefaults = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray *kidsDefaults = [[[NSMutableArray alloc] init] autorelease];

        for (int i = 0; i < 10; i++) {
            [primaryIncomeDefaults addObject:[NSNumber numberWithInt:40000]];
            [spouseIncomeDefaults addObject:[NSNumber numberWithInt:0]];
            [marriageDefaults addObject:[NSNumber numberWithBool:NO]];
            [kidsDefaults addObject:[NSNumber numberWithInt:0]];
        }
        
        [defaults setObject:primaryIncomeDefaults forKey:PRIMARY_INCOME_INPUT];
        [defaults setObject:spouseIncomeDefaults forKey:SPOUSE_INCOME_INPUT];
        [defaults setObject:marriageDefaults forKey:MARRIED_BY_YEAR];
        [defaults setObject:kidsDefaults forKey:KIDS_BY_YEAR];
                                                 
        sharedData.userInputs = defaults;
        
        sharedData.states = [DebtBombData createStates];
        
        NSString *congressFilePath = [[NSBundle mainBundle] 
                              pathForResource:@"json/states" ofType:@"json"];
        NSData *congressFileContent = [NSData dataWithContentsOfFile:congressFilePath];
        NSLog(@"%@",congressFileContent);
        NSDictionary *congressDict = [congressFileContent objectFromJSONData];
        sharedData.congressData = congressDict;
    });
    return sharedData;
}

- (NSDictionary*) getDataForState:(NSString*) stateName {
    NSArray *states = [self.congressData objectForKey:@"states"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"name = %@",[stateName uppercaseString]];
    NSArray *filteredStates = [states filteredArrayUsingPredicate:filter];
    return [filteredStates objectAtIndex:0];

}

+ (NSArray*) createStates {
    NSMutableArray* arrayStates = [[[NSMutableArray alloc] init] autorelease];
    [arrayStates addObject:@"Alabama"];
    [arrayStates addObject:@"Alaska"];
    [arrayStates addObject:@"Arizona"];
    [arrayStates addObject:@"Arkansas"];
    [arrayStates addObject:@"California"];
    [arrayStates addObject:@"Colorado"];
    [arrayStates addObject:@"Connecticut"];
    [arrayStates addObject:@"Delaware"];
    [arrayStates addObject:@"District of Columbia"];
    [arrayStates addObject:@"Florida"];
    [arrayStates addObject:@"Georgia"];
    [arrayStates addObject:@"Hawaii"];
    [arrayStates addObject:@"Idaho"];
    [arrayStates addObject:@"Illinois"];
    [arrayStates addObject:@"Indiana"];
    [arrayStates addObject:@"Iowa"];
    [arrayStates addObject:@"Kansas"];
    [arrayStates addObject:@"Kentucky"];
    [arrayStates addObject:@"Louisiana"];
    [arrayStates addObject:@"Maine"];
    [arrayStates addObject:@"Maryland"];
    [arrayStates addObject:@"Massachusetts"];
    [arrayStates addObject:@"Michigan"];
    [arrayStates addObject:@"Minnesota"];
    [arrayStates addObject:@"Mississippi"];
    [arrayStates addObject:@"Missouri"];
    [arrayStates addObject:@"Montana"];
    [arrayStates addObject:@"Nebraska"];
    [arrayStates addObject:@"Nevada"];
    [arrayStates addObject:@"New Hampshire"];
    [arrayStates addObject:@"New Jersey"];
    [arrayStates addObject:@"New Mexico"];
    [arrayStates addObject:@"New York"];
    [arrayStates addObject:@"North Carolina"];
    [arrayStates addObject:@"North Dakota"];
    [arrayStates addObject:@"Ohio"];
    [arrayStates addObject:@"Oklahoma"];
    [arrayStates addObject:@"Oregon"];
    [arrayStates addObject:@"Pennsylvania"];
    [arrayStates addObject:@"Rhode Island"];
    [arrayStates addObject:@"South Carolina"];
    [arrayStates addObject:@"South Dakota"];
    [arrayStates addObject:@"Tennessee"];
    [arrayStates addObject:@"Texas"];
    [arrayStates addObject:@"Utah"];
    [arrayStates addObject:@"Vermont"];
    [arrayStates addObject:@"Virginia"];
    [arrayStates addObject:@"Washington"];
    [arrayStates addObject:@"West Virginia"];
    [arrayStates addObject:@"Wisconsin"];
    [arrayStates addObject:@"Wyoming"];
    return arrayStates;
}


@end

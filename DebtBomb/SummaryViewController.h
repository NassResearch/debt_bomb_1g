//
//  SummaryViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel* extraTaxesLabel;
@property (nonatomic, strong) IBOutlet UIWebView* webView;

- (IBAction)showBackground:(id)sender;
- (IBAction)showContactCongress:(id)sender;

@end

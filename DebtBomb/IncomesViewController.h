//
//  IncomesViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IncomeTableViewCell.h"

#define PRIMARY_INCOME_TYPE @"PRIMARY_INCOME_TYPE"
#define SPOUSE_INCOME_TYPE @"SPOUSE_INCOME_TYPE"

@interface IncomesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) IBOutlet IncomeTableViewCell *incomeCell;
@property (nonatomic, strong) IBOutlet UINavigationItem *navItem;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString* incomeType;

- (IBAction)done:(id)sender;

@end

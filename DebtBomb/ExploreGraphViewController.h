//
//  ExploreGraphViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreGraphViewController : UIViewController
@property (nonatomic, strong) IBOutlet UINavigationBar *navBar;
- (IBAction)showNavBar:(id)sender;
- (IBAction)done:(id)sender;
@end

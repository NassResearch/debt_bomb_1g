//
//  CongressPersonViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CongressPersonViewController.h"
#import "SVWebViewController.h"


#define NAME_SECTION 0
#define PHONE_SECTION 1
#define EMAIL_SECTION 2
#define WEB_SECTION 3
#define ADDRESS_SECTION 4



@interface CongressPersonViewController (Private)
- (void) handlePhoneNumber:(NSString*)phoneNumber;
- (void) handleWebURL:(NSString*)webURL;

@end
@implementation CongressPersonViewController
@synthesize congressPerson = _congressPerson;
@synthesize headerView = _headerView;

- (void) dealloc {
    self.congressPerson = nil;
    self.headerView = nil;
    [super dealloc];
}
    
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Contact Info";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
/*    if (self.headerView == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"CongressPersonHeader" owner:self options:nil];
        self.headerView.nameLabel.text = [NSString stringWithFormat:@"%@ %@", 
                                     [contact objectForKey:@"name"],
                                     [contact objectForKey:@"familyname"]];
        if ([[contact allKeys] containsObject:@"pictureurl"]) {
            headerView.avatarView.image = [UIImage imageNamed:[contact objectForKey:@"pictureurl"]];
        }
    }
    [self.tableView setTableHeaderView: headerView];*/
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == NAME_SECTION) {
        return 1;
    }
    if (section == PHONE_SECTION) {
        return 2;
    }
    if (section == EMAIL_SECTION) {
        return 1;
    }
    if (section == WEB_SECTION) {
        return 1;
    }
    if (section == ADDRESS_SECTION) {
        return 1;
    }
    
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == NAME_SECTION) {
        static NSString *CellIdentifier = @"NameCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        }   
        return cell;
    } else if (indexPath.section == ADDRESS_SECTION) {
        static NSString *CellIdentifier = @"AddressCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        }
        //cell.textLabel.font.systemFontSize = 13;
        return cell;        
    
    } else {
        static NSString *CellIdentifier = @"LeftDetailCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:CellIdentifier] autorelease];
        }        
        if (indexPath.section == PHONE_SECTION) {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"DC";
                cell.detailTextLabel.text = [self.congressPerson objectForKey:@"dc_phone"];
            } else {
                cell.textLabel.text = @"local";
                cell.detailTextLabel.text = [self.congressPerson objectForKey:@"local_phone"];
            }
        }
        if (indexPath.section == EMAIL_SECTION) {
            cell.textLabel.text = @"email";
            cell.detailTextLabel.text = @"(tap for contact page)";
        }
        if (indexPath.section == ADDRESS_SECTION) {
            cell.textLabel.text = @"address";
            cell.detailTextLabel.text = [self.congressPerson objectForKey:@"physical_address"];
        }
        if (indexPath.section == WEB_SECTION) {
            cell.textLabel.text = @"web";
            cell.detailTextLabel.text = [self.congressPerson objectForKey:@"website"];
        }
        return cell;
    }

}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == ADDRESS_SECTION) {
        return 88;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}
/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == NAME_SECTION) {
        UIView
    }
    return [super tableView:tableView viewForHeaderInSection:section];
} */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == PHONE_SECTION) {
        if (indexPath.row == 0) {
            [self handlePhoneNumber:[self.congressPerson objectForKey:@"dc_phone"]];
           
        } else {
            [self handlePhoneNumber:[self.congressPerson objectForKey:@"local_phone"]];
        }
    }
    if (indexPath.section == EMAIL_SECTION) {
        [self handleWebURL:[self.congressPerson objectForKey:@"website_email"]];
    }

    if (indexPath.section == WEB_SECTION) {
        [self handleWebURL:[self.congressPerson objectForKey:@"website"]];
    }

}
- (void) handleWebURL:(NSString*)webURL {
    SVWebViewController *webViewController = [[SVWebViewController alloc] initWithAddress:webURL];
    [self.navigationController pushViewController:webViewController animated:YES];
    [webViewController release];
}
             
- (void) handlePhoneNumber:(NSString*)phoneNumber {
    NSString* phoneURL = [NSString stringWithFormat:@"tel://%@",phoneNumber];
    NSURL *URL = [NSURL URLWithString:phoneURL];
    [[UIApplication sharedApplication] openURL:URL];  
}

@end

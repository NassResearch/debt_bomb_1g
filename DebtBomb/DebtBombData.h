//
//  DebtBombData.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define AGE_IN_2011 @"AGE_IN_2011"
#define STATE_INDEX @"STATE_INDEX"
#define PRIMARY_INCOME_INPUT @"PRIMARY_INCOME_INPUT"
#define SPOUSE_INCOME_INPUT @"SPOUSE_INCOME_INPUT"
#define MARRIED_BY_YEAR @"MARRIED_BY_YEAR"
#define KIDS_BY_YEAR @"KIDS_BY_YEAR"

@interface DebtBombData : NSObject
+ (DebtBombData*) sharedInstance;

@property (nonatomic,strong) NSMutableDictionary *userInputs;
@property (nonatomic,strong) NSArray *states;
@property (nonatomic,strong) NSDictionary *congressData;

+ (NSArray*) createStates;
- (NSDictionary*) getDataForState:(NSString*) stateName;

@end

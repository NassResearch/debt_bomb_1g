//
//  GraphViewController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GraphViewController.h"
#import "ExploreGraphViewController.h"
#import "SummaryViewController.h"

@implementation GraphViewController

- (IBAction)showSummary:(id)sender {
    SummaryViewController *summaryViewController = [[[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil] autorelease];
    [self.navigationController pushViewController:summaryViewController animated:YES];
}

- (IBAction)exploreGraph:(id)sender {
    ExploreGraphViewController *exploreGraphViewController = [[[ExploreGraphViewController alloc] initWithNibName:@"ExploreGraphViewController" bundle:nil] autorelease];
    [self presentModalViewController:exploreGraphViewController animated:NO];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Taxes";
        UIBarButtonItem *rightButton = [[[UIBarButtonItem alloc] initWithTitle:@"Summary" style:UIBarButtonItemStylePlain target:self action:@selector(showSummary:)] autorelease];
        self.navigationItem.rightBarButtonItem = rightButton;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

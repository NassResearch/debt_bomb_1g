//
//  MainInputController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MainInputViewController.h"
#import "AgeViewController.h"
#import "StateViewController.h"
#import "IncomesViewController.h"
#import "DebtBombNotifications.h"
#import "FamilyInfoViewController.h"
#import "SummaryViewController.h"
#import "DebtBombData.h"


#define INPUTS_SECTION 0
#define CALCULATE_SECTION 1

#define AGE_ROW 0
#define STATE_ROW 1
#define PRIMARY_INCOME_ROW 2
#define FAMILY_INFORMATION_ROW 3
#define SPOUSE_INCOME_ROW 4

@implementation MainInputViewController


- (void) updateData:(NSNotification*) notification {
    [self.tableView reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Build a Future";
        self.tableView.delegate = self;
        self.tableView.dataSource = self;

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - Table Data Source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == INPUTS_SECTION) {
        return 5;
    } else if (section == CALCULATE_SECTION) {
        return 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == INPUTS_SECTION) {
        NSString *CellIdentifier = @"DetailTableCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row == AGE_ROW) {
            cell.textLabel.text = @"Age in 2011";
            NSNumber *age = [[[DebtBombData sharedInstance] userInputs] objectForKey:AGE_IN_2011];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",age];
        } else if (indexPath.row == STATE_ROW) {
            cell.textLabel.text = @"State";
            NSNumber *stateRow = [[[DebtBombData sharedInstance] userInputs] objectForKey:STATE_INDEX];
            cell.detailTextLabel.text = [StateViewController stateForRow:[stateRow intValue]]; 
        } else if (indexPath.row == PRIMARY_INCOME_ROW) {
            cell.textLabel.text = @"Primary Income";
            cell.detailTextLabel.text = @"";
        } else if (indexPath.row == FAMILY_INFORMATION_ROW) {
            cell.textLabel.text = @"Family Information";
            cell.detailTextLabel.text = @"";
        } else if (indexPath.row == SPOUSE_INCOME_ROW) {
            cell.textLabel.text = @"Spouse's Information";
            cell.detailTextLabel.text = @"";
        }
        return cell;
    } else if (indexPath.section == CALCULATE_SECTION) {
        NSString* CellIdentifier = @"ButtonTableCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.textLabel.text = @"Calculate";
        return cell;
    }
    return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

#pragma mark - Table Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 56;
    } else {
        return 56;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if (indexPath.section == INPUTS_SECTION) {
        if (indexPath.row == AGE_ROW) {
            AgeViewController* ageController = [[[AgeViewController alloc] initWithNibName:@"AgeViewController" bundle:nil] autorelease];
            ageController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:ageController animated:YES];
        }
        if (indexPath.row == STATE_ROW) {
            StateViewController* stateViewController = [[[StateViewController alloc] initWithNibName:@"StateViewController" bundle:nil] autorelease];
            stateViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:stateViewController animated:YES];
        }        
        if (indexPath.row == PRIMARY_INCOME_ROW) {
            IncomesViewController *primaryIncomesViewController = [[[IncomesViewController alloc] initWithNibName:@"IncomesViewController" bundle:nil] autorelease];
            primaryIncomesViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            primaryIncomesViewController.incomeType = PRIMARY_INCOME_TYPE;
            [self presentModalViewController:primaryIncomesViewController animated:YES];
        }
        if (indexPath.row == FAMILY_INFORMATION_ROW) {
            FamilyInfoViewController *familyInfoViewController = [[[FamilyInfoViewController alloc] initWithNibName:@"FamilyInfoViewController" bundle:nil] autorelease];
            familyInfoViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:familyInfoViewController animated:YES];
        }
        if (indexPath.row == SPOUSE_INCOME_ROW) {
            IncomesViewController *spouseIncomesViewController = [[[IncomesViewController alloc] initWithNibName:@"IncomesViewController" bundle:nil] autorelease];
            spouseIncomesViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            spouseIncomesViewController.incomeType = SPOUSE_INCOME_TYPE;
            [self presentModalViewController:spouseIncomesViewController animated:YES];
        }
    } else if (indexPath.section == CALCULATE_SECTION) {
        SummaryViewController *summaryViewController = [[[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil] autorelease];
        [self.navigationController pushViewController:summaryViewController animated:YES];
    }
     
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:AGE_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateData:) name:STATE_CHANGED object:nil];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = FALSE;

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end

//
//  AgeController.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AgeViewController.h"
#import "DebtBombData.h"
#import "DebtBombNotifications.h"

#define MAX_AGE 99
#define MIN_AGE 18

@implementation AgeViewController

@synthesize ageLabel = _ageLabel;
@synthesize agePicker = _agePicker;


- (void) dealloc {
    self.agePicker = nil;
    self.ageLabel = nil;
}

- (IBAction)done:(id)sender {
    //save picked value
    NSInteger selectedAge = [self.agePicker selectedRowInComponent:0] + MIN_AGE;
    NSNumber* pickedValue = [NSNumber numberWithInt:selectedAge];
    [[[DebtBombData sharedInstance] userInputs] setObject:pickedValue forKey:AGE_IN_2011];
    
    //tell anyone listening
    [[NSNotificationCenter defaultCenter] postNotificationName:AGE_CHANGED object:self];
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void) changeAgeLabelText:(NSInteger)age {
    self.ageLabel.text = [NSString stringWithFormat:@"Age in 2011: %d",age];
}


#pragma mark - UIPickerView Data Source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return MAX_AGE - MIN_AGE + 1;
}


#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%d",MIN_AGE + row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self changeAgeLabelText:row + MIN_AGE];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSInteger age = [[[[DebtBombData sharedInstance] userInputs] objectForKey:AGE_IN_2011] intValue]; 
    [self changeAgeLabelText:age];
    [self.agePicker selectRow:age - MIN_AGE inComponent:0 animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

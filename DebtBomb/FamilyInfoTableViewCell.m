//
//  FamilyInfoTableViewCell.m
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FamilyInfoTableViewCell.h"
#import "DebtBombData.h"
#import "DebtBombNotifications.h"

@implementation FamilyInfoTableViewCell

@synthesize row = _row;
@synthesize marriedSwitch = _marriedSwitch;
@synthesize childStepper = _childStepper;
@synthesize yearLabel = _yearLabel;

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.childStepper = nil;
    self.marriedSwitch = nil;
    self.yearLabel = nil;    
}


- (IBAction)kidsValueChanged:(id)sender {
    NSMutableArray *kids = (NSMutableArray*)[[[DebtBombData sharedInstance] userInputs] objectForKey:KIDS_BY_YEAR];
    [kids replaceObjectAtIndex:self.row withObject:[NSNumber numberWithInt:self.childStepper.value]];
    [[NSNotificationCenter defaultCenter] postNotificationName:KIDS_CHANGED object:self];
}
- (IBAction)marriageChanged:(id)sender {
    NSMutableArray *marriage = (NSMutableArray*)[[[DebtBombData sharedInstance] userInputs] objectForKey:MARRIED_BY_YEAR];
    [marriage replaceObjectAtIndex:self.row withObject:[NSNumber numberWithBool:[self.marriedSwitch isOn]]];
    [[NSNotificationCenter defaultCenter] postNotificationName:MARRIAGE_CHANGED object:self];
}

- (void) marriageNotification:(NSNotification*)notification {
    FamilyInfoTableViewCell* cell = (FamilyInfoTableViewCell*)notification.object;
    if (cell.row < self.row) {
        [self.marriedSwitch setOn:[cell.marriedSwitch isOn]];
    }
}

- (void) kidsNotification:(NSNotification*)notification {
    FamilyInfoTableViewCell* cell = (FamilyInfoTableViewCell*)notification.object;
    if (cell.row < self.row) {
        self.childStepper.value = cell.childStepper.value;
        
    }
}

- (void)startListeningToNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(marriageNotification:) name:MARRIAGE_CHANGED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(kidsNotification:) name:KIDS_CHANGED object:nil];
}

@end

//
//  BackgroundViewController.h
//  DebtBomb
//
//  Created by Jeffrey Linwood on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackgroundViewController : UIViewController
@property (nonatomic, strong) IBOutlet UIWebView *webView;

- (IBAction)done:(id)sender;
@end
